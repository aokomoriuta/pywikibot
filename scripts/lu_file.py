#!/usr/bin/python
# -*- coding: utf-8 -*-
import pywikibot
import io, sys

def main(*args):
	sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')

	print(u"= ファイルのライセンス更新判定 =")
	site = pywikibot.Site()
	cat = pywikibot.Category(site,"Category:ライセンス更新対象外かもしれないファイル")

	for file in cat.articles(namespaces=6):
		if not "{{self" in file.text:
			timestamps = file.get_file_history().keys()
			if len(timestamps) == 1:
				print("* ", file, "non-self, only 1 file")
				time = str(max(timestamps))
				date = time[0:10]
				file.text = file.text.replace("{{GFDL", "{{{{GFDL|{{{{ライセンス更新判定|1.2以降|なし|{0}|???|自動判定不可|--~~~~}}}}".format(date))
				file.save(u"[[Wikipedia:Bot作業依頼/ファイルのライセンス更新判定]]")
				break

if __name__ == "__main__":
    main()
